let is_equal a b = 
	a = b

let ft_is_palindrome str =
	let len = ((String.length str)) 
	in
		let rec loop start =
			if start <= len / 2
			then
				is_equal str.[start] str.[len -1 -start] && loop (start+1) 
			else
				true
		in loop 0
	

let main () =
	let print_bool value =
	if value
	then print_endline "True"
 	else 
		print_endline "False"
	in
	print_bool(ft_is_palindrome "radar");
	print_bool(ft_is_palindrome "madam");
	print_bool(ft_is_palindrome "mad");
	print_bool(ft_is_palindrome "adda")

(*********************)
let () = main ()