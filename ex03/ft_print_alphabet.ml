let ft_print_alphabet () =
	let a = int_of_char 'a' in
	let z = int_of_char 'z' in
	let rec loop a z =
		if a < z
		then begin print_char (char_of_int a); loop (a + 1) z end
		else print_char (char_of_int a)
	in
	loop a z ; print_char '\n'


let () = ft_print_alphabet ()
		
