let rec ft_print str i =
	if (i >= 0)
	then
		begin
			print_char (str.[i]);
			ft_print str (i-1)
		end

let ft_print_rev str =
	let i = ((String.length str) - 1) in 
	ft_print str i;
	print_char '\n'


let main () =
	ft_print_rev "Hello !";
	ft_print_rev "abcdefghij";
	ft_print_rev ""

(* * * * * * * * * * * * * * * *)
let () = main ()