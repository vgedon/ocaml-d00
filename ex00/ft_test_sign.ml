let ft_test_sign x =
	if x < 0
		then print_endline "negative"
		else print_endline "positive"

let () = print_string "Test [";print_int 42; print_string "] : " ; ft_test_sign 42 ;
 print_string "Test [0] : " ; ft_test_sign 0 ;
 print_string "Test [-42] : " ; ft_test_sign (-42)
