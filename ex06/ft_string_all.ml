let is_ascii c =
	c >= '0' && c <= '9'

let ft_string_all f str =
	let rec loop i =
		if i < (String.length str) 
		then f (String.get str i) && loop (i + 1) 
		else true
	in
	loop 0


let () = print_endline (string_of_bool (ft_string_all is_ascii "0123456789"))
let () = print_endline (string_of_bool (ft_string_all is_ascii "01234QWE56789"))

(* 
let plus_three x = x + 3
let twice (f, z) = f (f z)
twice (plus_three, 5)
 *)