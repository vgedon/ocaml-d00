let ft_print_comb () =
	let rec loop a =
		if a <= 7
		then begin
		let rec loop2 b =
			if b <= 8
			then begin
			let rec loop3 c =
				if c <= 9
				then begin
				print_int a;
				print_int b;
				print_int c;
		if a <> 7
		then
			print_char ',';
			print_char ' ';
		loop3 (c + 1);
				end
			in
			loop3 (b + 1);
			loop2 (b + 1)
			end
		in
		loop2(a + 1);
		loop (a + 1)
		end
	in loop 0;
	print_char '\n'

let () = ft_print_comb ()
