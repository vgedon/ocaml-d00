let rec ft_power n p =
	if p > 0
	then n * ft_power n ( p - 1 )
	else 1

let () = print_string "10 ^ 2 = " ; print_int (ft_power 10 2) ; print_char '\n'
let () = print_string "10 ^ 0 = " ; print_int (ft_power 10 0) ; print_char '\n'
let () = print_string "0 ^ 2 = " ;print_int (ft_power 0 2) ; print_char '\n'
